# Start with a base image containing Java runtime
FROM openjdk:13-jdk-alpine

# Add Maintainer Info
LABEL maintainer=""

# Add a volume pointing to /tmp
VOLUME /tmp

# Port expose
EXPOSE 8020

# The application's args
ARG VERSION=1.0.0

# Add the application's jar to the container
ADD target/userOld-service-$VERSION.jar userOld-service.jar

# Run the jar file
ENTRYPOINT exec java  \
 -Djava.security.egd=file:/dev/./urandom \
 -Dspring.profiles.active=$PROFILE \
 -jar userOld-service.jar