package it.piucultura.backend.userservice.config;


import java.lang.annotation.*;

/**
 * Spring security fornisce una annotation chiamata @AuthenticationPrincipal per accedere dai controllers
 * all'utente corrente autenticato. Questa annotation "CurrentUser" è sviluppata intorno alla @AuthenticationPrincipal.
 * Questa meta-annotation server a facilitare l'utilizzo delle annotation di spring security. Se si decide di
 * togliere spring security noi possiamo semplicemente modificare questa meta-annotation piuttosto che cambiare tutte le
 * classi del progetto.
 */
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CurrentUser {

}
