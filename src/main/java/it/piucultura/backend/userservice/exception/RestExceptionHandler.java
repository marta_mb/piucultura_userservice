package it.piucultura.backend.userservice.exception;


import it.piucultura.backend.userservice.modelDtos.response.ResponseModel;
import it.piucultura.backend.userservice.modelDtos.response.UserResponse;
import it.piucultura.backend.userservice.utility.CodiciErrore;
import javassist.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends  ResponseEntityExceptionHandler
{
    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ResponseModel> exceptionNotFoundHandler(Exception ex)
    {
        ResponseEntity<ResponseModel> responseResponseEntity = UserResponse.HTTP_ERROR_USER(ex.getMessage(), CodiciErrore.NOT_FOUND);
        return responseResponseEntity;
    }
    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<ResponseModel> exceptionUserNotFoundHandler(Exception ex)
    {
        ResponseEntity<ResponseModel> responseResponseEntity = UserResponse.HTTP_ERROR_USER(ex.getMessage(), CodiciErrore.USER_NOT_FOUND);
        return responseResponseEntity;
    }
    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<ResponseModel> exceptionResourcesNotFoundHandler(Exception ex)
    {
        ResponseEntity<ResponseModel> responseResponseEntity = UserResponse.HTTP_ERROR_USER(ex.getMessage(), CodiciErrore.USER_NOT_FOUND);
        return responseResponseEntity;
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseModel> exceptionHandler(Exception ex)
    {
        return UserResponse.HTTP_ERROR_USER(ex.getMessage(),CodiciErrore.GENERIC_EXC);
    }

}
