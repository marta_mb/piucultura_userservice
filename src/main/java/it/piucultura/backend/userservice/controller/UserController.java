package it.piucultura.backend.userservice.controller;


import io.swagger.annotations.*;
import it.piucultura.backend.userservice.exception.UserNotFoundException;
import it.piucultura.backend.userservice.modelDtos.StringModel;
import it.piucultura.backend.userservice.modelDtos.dto.UserPrincipalDto;
import it.piucultura.backend.userservice.modelDtos.request.RequestCambioPassword;
import it.piucultura.backend.userservice.modelDtos.response.ResponseModelString;
import it.piucultura.backend.userservice.modelDtos.response.ResponseUserPrincipal;
import it.piucultura.backend.userservice.security.jwt.JwtTokenProvider;
import it.piucultura.backend.userservice.utility.CodiciErrore;
import it.piucultura.backend.userservice.entity.UserPrincipal;
import it.piucultura.backend.userservice.repository.UserRepository;
import it.piucultura.backend.userservice.modelDtos.response.UserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Transactional
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);



    @ApiOperation(
            value = "Restituisce il profilo utente",
            notes = "oggetto restituito   ResponseUserPrincipal",
            response = ResponseUserPrincipal.class,
            produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "Token", paramType = "header"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @PostMapping("/userinfo/")
    public ResponseEntity<ResponseUserPrincipal> getCurrentUser(@RequestHeader(value = "token") HttpHeaders headers, @RequestBody @ApiParam(value = "model con email utente da recuperare") StringModel username) throws UserNotFoundException {
        List<String> token = headers.get("token");

        Long userIdFromJwt = tokenProvider.getUserIdFromJwt(token.get(0));
        Optional<UserPrincipal> tokenUser = userRepository.findById(userIdFromJwt);


        if( !tokenUser.get().getUsername().equals(username.getValue()) ) {
            String ErrMsg = String.format("Token non valido", tokenUser.get().getUsername());
            throw  new UserNotFoundException(ErrMsg);
        }

        Optional<UserPrincipal> byUsername = userRepository.findByUsernameIsLike(username.getValue());

        if(!byUsername.isPresent()) {
            String ErrMsg = String.format("Utente %s non trovato!", byUsername.get().getEmail());
            throw  new UserNotFoundException(ErrMsg);
        }
        UserPrincipalDto userPrincipalDto = UserPrincipalDto.create(byUsername.get());
        return UserResponse.HTTP_OK_USER_PRINCIPAL_RESPONSE(userPrincipalDto);
    }

    @ApiOperation(
            value =  "Questo servizio viene chiamato quando un utente (registrato e loggato) richiede il cambio password.",
            notes = "oggetto restituito   ResponseUserPrincipal",
            response = ResponseEntity.class,
            produces = "application/json")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "Token", paramType = "header"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @PostMapping("/changepassword/")
    public ResponseEntity<?> resetPassword(@RequestHeader(value = "token") HttpHeaders headers , @RequestBody @ApiParam(value = "Richiesta cambio password") RequestCambioPassword requestCambioPassword)  {
        List<String> token = headers.get("token");

        Long userIdFromJwt = tokenProvider.getUserIdFromJwt(token.get(0));
        Optional<UserPrincipal> tokenUser = userRepository.findById(userIdFromJwt);


        if( !tokenUser.get().getUsername().equals(requestCambioPassword.getUsername()) ) {
            return UserResponse.HTTP_ERROR_USER("Token non valido", CodiciErrore.CHPS_USERNAME_ERROR);
        }

        Optional<UserPrincipal> byUsername = userRepository.findByUsernameIsLike(requestCambioPassword.getUsername());
        UserPrincipalDto currentUser = UserPrincipalDto.create(byUsername.get());

        if( !currentUser.getUsername().equals(requestCambioPassword.getUsername()) ) {
            return UserResponse.HTTP_ERROR_USER("L'username non corrisponde. Inserisci di nuovo il tuo username e riprova!", CodiciErrore.CHPS_USERNAME_ERROR);
        }

        System.out.println("OLD: " + currentUser.getPassword());
        System.out.println("OLD FORM: " + passwordEncoder.encode(requestCambioPassword.getOldPassword()));


        //if( !currentUser.getPassword().equals(passwordEncoder.encode(requestCambioPassword.getOldPassword())) ) {
        if(!passwordEncoder.matches(requestCambioPassword.getOldPassword(),currentUser.getPassword())) {
            return UserResponse.HTTP_ERROR_USER("La vecchia password non corrisponde!", CodiciErrore.CHPS_OLD_PSW_ERROR);
        }

        if(passwordEncoder.matches(requestCambioPassword.getNewPassword(),currentUser.getPassword())) {
        //if( !currentUser.getPassword().equals(passwordEncoder.encode(requestCambioPassword.getNewPassword())) ) {
            return UserResponse.HTTP_ERROR_USER("La nuova password non può essere uguale alla vecchia password!", CodiciErrore.CHPS_NEW_PSW_ERROR);
        }



        UserPrincipal userPrincipal = new UserPrincipal();

        BeanUtils.copyProperties(currentUser,userPrincipal);
        userPrincipal.setConfirmEmail(true);
        userPrincipal.setPassword(passwordEncoder.encode(requestCambioPassword.getNewPassword()));

        UserPrincipal save = userRepository.save(userPrincipal);

        return UserResponse.HTTP_OK_USER_CAMBIO_PS_RESPONSE("Password cambiata con successo!");

    }



}
