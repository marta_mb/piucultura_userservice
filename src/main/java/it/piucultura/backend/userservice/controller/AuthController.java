package it.piucultura.backend.userservice.controller;



import io.swagger.annotations.*;
import it.piucultura.backend.userservice.entity.*;
import it.piucultura.backend.userservice.exception.UserNotFoundException;
import it.piucultura.backend.userservice.modelDtos.StringModel;
import it.piucultura.backend.userservice.modelDtos.dto.JwtAuthenticationDTO;
import it.piucultura.backend.userservice.modelDtos.dto.UserPrincipalDto;
import it.piucultura.backend.userservice.modelDtos.request.RequestLogin;
import it.piucultura.backend.userservice.modelDtos.request.RequestResetPassword;
import it.piucultura.backend.userservice.modelDtos.request.RequestSignUp;
import it.piucultura.backend.userservice.modelDtos.response.*;
import it.piucultura.backend.userservice.repository.DisabilityRepository;
import it.piucultura.backend.userservice.utility.CodiciErrore;
import it.piucultura.backend.userservice.exception.AppException;
import it.piucultura.backend.userservice.repository.GroupsRepository;
import it.piucultura.backend.userservice.repository.UserRepository;
import it.piucultura.backend.userservice.security.jwt.JwtTokenProvider;
import it.piucultura.backend.userservice.service.UserServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/auth")
@EnableSwagger2

public class AuthController {
    @Autowired
    private UserServiceImpl userService;

    @Autowired
    public JavaMailSender emailSender;


    @Autowired
    UserRepository userRepository;

    @Autowired
    GroupsRepository groupsRepository;

    @Autowired
    DisabilityRepository disabilityRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Value("${appmvc.url}")
    String appUrl;


    @ApiOperation(
            value = "Restituisce l'oggetto ResponseJwtAuthentication per l'autenticazione dell'utente, controlla se l'utente è registrato e se l'email è stata confermata",
            notes = "oggetto restituito   ResponseEntity<ResponseJwtAuthentication>",
            response = ResponseJwtAuthentication.class,
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @PostMapping("/signin")
    public ResponseEntity<ResponseJwtAuthentication> authenticateUser(@Valid @RequestBody @ApiParam(value = "Richiesta login") RequestLogin loginRequest) throws UserNotFoundException {

        Optional<UserPrincipal> byUsername = userRepository.findByUsernameIsLike(loginRequest.getUsernameOrEmail());

        if(!byUsername.isPresent()) {
            String ErrMsg = String.format("utente %s non trovato!", loginRequest.getUsernameOrEmail());
            throw  new UserNotFoundException(ErrMsg);
        }

        if(byUsername.get().getConfirmEmail() == null || !byUsername.get().getConfirmEmail()){
            return UserResponse.HTTP_ERROR_USER_JWT("Email non confermata","EMAIL_NOT_CONF_01");
        }

        String jwt = tokenProvider.generateToken(byUsername.get().getId());

        return UserResponse.HTTP_OK_USER_JWT_AUTH_RESPONSE(new JwtAuthenticationDTO(jwt));
    }


    @ApiOperation(
            value = "Valida il token",
            notes = "oggetto restituito   ResponseEntity<?>",
            response = ResponseEntity.class,
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @PostMapping("/validate/")
    public ResponseEntity<?> validateToken(@Valid @RequestBody @ApiParam(value = "Token da validare") JwtAuthenticationDTO token){

        Boolean validate = tokenProvider.validateToken(token.getAccessToken());

        return  ResponseEntity.ok(validate);
    }

    @ApiOperation(
            value = "Registrazione nuovo utente",
            notes = "oggetto restituito   ResponseEntity<ResponseUserPrincipal>",
            response = ResponseUserPrincipal.class,
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @PostMapping("/signup")
    public ResponseEntity<ResponseUserPrincipal> registerUser(@Valid @RequestBody @ApiParam(value = "Richiesta login") RequestSignUp requestSignUp) {
        if(requestSignUp.getId()==null) {
            if (userRepository.existsByUsername(requestSignUp.getUsername())) {
                return UserResponse.HTTP_ERROR_USER_REG("L'username scelto è già in uso da un altro utente!", CodiciErrore.AUTH_UNAUTHORIZED);
            }

            if (userRepository.existsByEmail(requestSignUp.getEmail())) {
                return UserResponse.HTTP_ERROR_USER_REG("L'ndirizzo email inserito è già in uso su un altro account", CodiciErrore.AUTH_UNAUTHORIZED);
            }
        }
        UserPrincipal userPrincipal = new UserPrincipal();

        /*Group userGroup = groupsRepository.findByName(GroupName.ROLE_USER)
                .orElseThrow(() -> new AppException("UserPrincipal Group not found on database!"));

        Disabilita userDisability = disabilityRepository.findByName(DisabilityName.NESSUNA)
                .orElseThrow(() -> new AppException("Disabilità Group not found on database!"));*/

        BeanUtils.copyProperties(requestSignUp,userPrincipal);
/*
        userPrincipal.setGroups(Collections.singleton(userGroup));
*/

        if(requestSignUp.getId()==null) {
            userPrincipal.setConfirmEmail(false);
            userPrincipal.setConfirmToken(UUID.randomUUID().toString());
        }

   /*     userPrincipal.setDisability(Collections.singleton(userDisability));*/

        UserPrincipal result = userRepository.save(userPrincipal);


        if(requestSignUp.getId()==null) {
            sendEmail(userPrincipal.getEmail(), "Conferma la tua email", "Per confermare la tua email clicca sul link qui sotto:\n" + appUrl
                    + "/auth/userpassword?token=" + userPrincipal.getConfirmToken());
        }
        UserPrincipalDto userPrincipalDto = UserPrincipalDto.create(result);
        return UserResponse.HTTP_OK_USER_PRINCIPAL_RESPONSE(userPrincipalDto);

    }

    @ApiOperation(
            value = "Dopo la registrazione viene inviata una mail all'indirizzo indicato dall'utente, cliccando sul link ricevuto viene richiamato questo servizio che valida l'account di posta elettronica",
            notes = "oggetto restituito   ResponseEntity",
            response = ResponseUserPrincipal.class,
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    // Process form submission from forgotPassword page
    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public ResponseEntity<ResponseUserPrincipal>  processConfirmEmail(@RequestParam("token") @ApiParam(value = "Token confirm") StringModel tokenConfirmEmail)throws UserNotFoundException {
        System.out.println("confirm token");
        UserPrincipalDto userPrincipalDto = userService.loadUserByConfirmToken(tokenConfirmEmail.getValue());
        Optional<UserPrincipal> byUsername = userRepository.findByUsernameIsLike(userPrincipalDto.getUsername());
        if(!byUsername.isPresent()){
            String ErrMsg = String.format("L'utente %s non è stato trovato!", byUsername.get().getEmail());
            throw  new UserNotFoundException(ErrMsg);
        }

        UserPrincipal userPrincipal = new UserPrincipal();
        BeanUtils.copyProperties(byUsername.get(),userPrincipal);

        userPrincipal.setConfirmToken(null);
        userPrincipal.setConfirmEmail(true);
        userRepository.save(userPrincipal);


        return UserResponse.HTTP_OK_USER_PRINCIPAL_RESPONSE(UserPrincipalDto.create(userPrincipal));
    }

    @ApiOperation(
            value = "Questo servizio viene chiamato quando un utente registrato ha dimenticato la password. Controlla l'esistenza dell'utente ed invia l'email per il reset della password all'email associata ",
            notes = "oggetto restituito   ResponseCambioPassword",
            response = ResponseCambioPassword.class,
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @RequestMapping(value = "/forgot/", method = RequestMethod.POST)
    public ResponseEntity<ResponseCambioPassword>  processForgotPasswordForm(@RequestBody @ApiParam(value = "Email associata all'account di cui recuperare la password") StringModel userEmail) throws UserNotFoundException {

        // Lookup user in database by e-mail
        UserPrincipalDto userPrincipalDto = userService.loadUserByUsername(userEmail.getValue());

        if (userPrincipalDto == null) {
            return   UserResponse.HTTP_ERROR_USER_CAMBIO_PS_RESPONSE("Utente non trovato",CodiciErrore.AUTH_UNMATCH_ISSURER);

        } else {

            // Generate random 36-character string token for reset password
            String username = userPrincipalDto.getUsername();
            Optional<UserPrincipal> byUsername = userRepository.findByUsernameIsLike(username);
            if(!byUsername.isPresent()) {
                String ErrMsg = String.format("L'utente %s non è stato trovato!",username);
                throw  new UserNotFoundException(ErrMsg);
            }

            UserPrincipal userPrincipal = byUsername.get();
            userPrincipal.setResetToken(UUID.randomUUID().toString());

            // Save token to database
            UserPrincipal save = userRepository.save(userPrincipal);



            sendEmail(save.getEmail(),"Più cultura reset password","Ciao " + save.getUsername() + ",\n"+ "Per resettare la tua password, clicca sul link qui sotto:\n\n" + appUrl
                    + "/auth/reset?token=" + userPrincipal.getResetToken());

            return   UserResponse.HTTP_OK_USER_CAMBIO_PS_RESPONSE("Il link per resettare la password è stato inviato all'indirizzo " + userEmail.getValue());

        }
        }

    @ApiOperation(
            value = "Dopo che un utente registrato ha chiesto il reset della password riceve un email contenente un link." +
                    "Questo servizio viene richiamato quando l'utente clicca sul link ricevuto nella email",
            notes = "oggetto restituito   ResponseCambioPassword",
            response = ResponseCambioPassword.class,
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public ResponseEntity<ResponseCambioPassword>  processResetPasswordForm(@RequestBody @ApiParam(value = "Token associato all'utente che vuole recuperare la password") RequestResetPassword tokenUserResetPassword) throws UserNotFoundException {

        System.out.println("processResetPasswordForm");
            UserPrincipalDto userPrincipalDto = userService.loadUserByResetToken(tokenUserResetPassword.getToken());

            if(userPrincipalDto == null) {
            String ErrMsg = String.format("L'utente non è stato trovato!");
            throw  new UserNotFoundException(ErrMsg);
        }
        UserPrincipal userPrincipal = new UserPrincipal();

        BeanUtils.copyProperties(userPrincipalDto,userPrincipal);
        userPrincipal.setPassword((passwordEncoder.encode(tokenUserResetPassword.getNewPassword())));
        userPrincipal.setConfirmEmail(true);
        userRepository.save(userPrincipal);


        return UserResponse.HTTP_OK_USER_CAMBIO_PS_RESPONSE("Password cambiata con successo!");
    }



    @ApiOperation(
            value = "Questo servizio viene chiamato dopo aver cliccato sul link ricevuto per email e compilato il form di impostazione password a cui si viene reindirizzati.",
            notes = "oggetto restituito   ResponseCambioPassword",
            response = ResponseCambioPassword.class,
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @RequestMapping(value = "/savepassword", method = RequestMethod.POST)
    public ResponseEntity<ResponseCambioPassword>  processSavePasswordForm(@RequestBody @ApiParam(value = "Token associato all'utente") RequestResetPassword tokenUserResetPassword) throws UserNotFoundException {

        System.out.println("processResetPasswordForm");
        UserPrincipalDto userPrincipalDto = userService.loadUserByConfirmToken(tokenUserResetPassword.getToken());

        if(userPrincipalDto == null) {
            String ErrMsg = String.format("L'utente non è stato trovato!");
            throw  new UserNotFoundException(ErrMsg);
        }
        UserPrincipal userPrincipal = new UserPrincipal();

        BeanUtils.copyProperties(userPrincipalDto,userPrincipal);
        userPrincipal.setPassword((passwordEncoder.encode(tokenUserResetPassword.getNewPassword())));
        userPrincipal.setConfirmEmail(true);

        userRepository.save(userPrincipal);


        return UserResponse.HTTP_OK_USER_CAMBIO_PS_RESPONSE("Password cambiata con successo!");
    }


    private void sendEmail(String to, String subject, String textMessage){

        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(to);
        message.setSubject(subject);
        message.setText(textMessage);
        emailSender.send(message);
    }

    @ApiOperation(
            value = "Questo servizio viene chiamato quando un utente si è già registrato, ma non ha ricevuto l'email di conferma account ",
            notes = "oggetto restituito   ResponseCambioPassword",
            response = ResponseModelString.class,
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Il servizio ha risposto in maniera corretta"),
            @ApiResponse(code = 401, message = "Permessi negati"),
            @ApiResponse(code = 403, message = "Autenticazione fallita"),
    })
    @PostMapping("/resend")
    public ResponseEntity<ResponseModelString> registerUser(@Valid @RequestBody @ApiParam(value = "Email dove inviare il link per l'attivazione dell'account") StringModel requestSignUp) throws UserNotFoundException {

            if (!userRepository.findByEmailIsLike(requestSignUp.getValue()).isPresent()) {
                String ErrMsg = String.format("Utente non trovato!");
                throw  new UserNotFoundException(ErrMsg);
            }
        // Creating userPrincipal's account

        Optional<UserPrincipal> byEmailIsLike = userRepository.findByEmailIsLike(requestSignUp.getValue());

        String message = "Email inviata";

        if(byEmailIsLike.get().getConfirmEmail()==false) {

            sendEmail(requestSignUp.getValue(), "Conferma la tua email", "Per confermare la tua email clicca sul link qui sotto:\n" + appUrl
                    + "/auth/userpassword?token=" + byEmailIsLike.get().getConfirmToken());

        } else {
            String ErrMsg = String.format("Questo account è gia stato confermato");
            throw  new UserNotFoundException(ErrMsg);
        }
        ResponseModelString modelString = new ResponseModelString();
        StringModel stringModel = new StringModel();
        stringModel.setValue(message);
        return UserResponse.HTTP_OK_USER_STRING_MODEL(stringModel);
    }

}




