package it.piucultura.backend.userservice.security.jwt;

import io.jsonwebtoken.*;
import it.piucultura.backend.userservice.modelDtos.dto.UserPrincipalDto;
import it.piucultura.backend.userservice.entity.UserPrincipal;
import it.piucultura.backend.userservice.repository.GroupsRepository;
import it.piucultura.backend.userservice.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

/***
 * Classe di utility che sarà usata per generare un JWT dopo che un utente si è correttamente loggato, e per validare
 * il JWT inviato nell'header delle request. La stringa jwtSecret e la durata del token, saranno lette
 * dal file application.properties
 */
@Component
public class JwtTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Autowired
    GroupsRepository groupsRepository;

    @Autowired
    UserRepository userRepository;


    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;


    public String generateToken(Long idUtente) {

        Optional<UserPrincipal> user = userRepository.findById(idUtente);
        if(!user.isPresent()){
            return "";
        }

        Date now = new Date();
        Date expireDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder()
                .setSubject(Long.toString(idUtente))
                .setIssuedAt(new Date())
                .claim("username", user.get().getUsername())
                .claim("name", user.get().getName())
               // .claim("groups", user.get().getGroups())
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }


    public Long getUserIdFromJwt(String token) {

        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }


    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty");
        }

        return false;
    }
}
