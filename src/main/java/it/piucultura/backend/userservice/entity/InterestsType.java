package it.piucultura.backend.userservice.entity;

public enum InterestsType {

    ARTE_CONTEMPORANEA,
    SCULTURA,
    ARTE_CLASSICA,
    SITI_CULTURALI,
    MONUMENTI,
    STORIA
}
