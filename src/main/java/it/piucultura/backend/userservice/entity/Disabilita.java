package it.piucultura.backend.userservice.entity;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "disability")
public class Disabilita {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(length = 60)
    private DisabilityName name;

    @Size(max = 40)
    @Column(name = "altre_disabilita")
    private String altreDisalitita;

    public Disabilita() {
    }

    public Disabilita(DisabilityName name, @NotBlank @Size(max = 40) String altreDisalitita) {
        this.name = name;
        this.altreDisalitita = altreDisalitita;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DisabilityName getName() {
        return name;
    }

    public void setName(DisabilityName name) {
        this.name = name;
    }

    public String getAltreDisalitita() {
        return altreDisalitita;
    }

    public void setAltreDisalitita(String altreDisalitita) {
        this.altreDisalitita = altreDisalitita;
    }

    @Override
    public String toString() {
        return "Disabilita{" +
                "id=" + id +
                ", name=" + name +
                ", altreDisalitita='" + altreDisalitita + '\'' +
                '}';
    }
}
