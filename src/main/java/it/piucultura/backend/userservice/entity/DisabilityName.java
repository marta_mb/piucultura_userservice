package it.piucultura.backend.userservice.entity;

public enum DisabilityName {

    NESSUNA,
    NON_VEDENTE,
    NON_UDENTE,
    PARAPLEGICO
}
