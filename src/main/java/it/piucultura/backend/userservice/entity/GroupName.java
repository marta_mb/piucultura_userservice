package it.piucultura.backend.userservice.entity;

public enum GroupName {

    ROLE_USER,
    ROLE_ADMIN
}
