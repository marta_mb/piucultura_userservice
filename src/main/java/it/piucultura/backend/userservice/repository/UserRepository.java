package it.piucultura.backend.userservice.repository;


import it.piucultura.backend.userservice.entity.UserPrincipal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserPrincipal, Long> {

    Optional<UserPrincipal> findByEmailIsLike(String email);

    Optional<UserPrincipal> findByUsernameOrEmail(String username, String email);

    List<UserPrincipal> findByIdIn(List<Long> userIds);

    Optional<UserPrincipal> findByUsernameIsLike(String username);

    Optional<UserPrincipal> getByUsername(String username);

    Optional<UserPrincipal> findByUsernameIs(String username);

    Optional<UserPrincipal> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Optional<UserPrincipal> findByResetToken(String resetToken);

    Optional<UserPrincipal> findByConfirmToken(String confirmToken);


}
