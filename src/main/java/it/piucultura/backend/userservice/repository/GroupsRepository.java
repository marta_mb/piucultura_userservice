package it.piucultura.backend.userservice.repository;



import it.piucultura.backend.userservice.entity.Group;
import it.piucultura.backend.userservice.entity.GroupName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupsRepository extends JpaRepository<Group, Long> {
    Optional<Group> findByName(GroupName groupName);
}
