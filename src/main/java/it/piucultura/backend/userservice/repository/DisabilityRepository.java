package it.piucultura.backend.userservice.repository;



import it.piucultura.backend.userservice.entity.Disabilita;
import it.piucultura.backend.userservice.entity.DisabilityName;
import it.piucultura.backend.userservice.entity.Group;
import it.piucultura.backend.userservice.entity.GroupName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DisabilityRepository extends JpaRepository<Disabilita, Long> {
    Optional<Disabilita> findByName(DisabilityName disabilityName);
}
