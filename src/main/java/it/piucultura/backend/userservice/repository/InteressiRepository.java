package it.piucultura.backend.userservice.repository;



import it.piucultura.backend.userservice.entity.Group;
import it.piucultura.backend.userservice.entity.GroupName;
import it.piucultura.backend.userservice.entity.Interessi;
import it.piucultura.backend.userservice.entity.InterestsType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InteressiRepository extends JpaRepository<Interessi, Long> {
    Optional<Interessi> findByName(InterestsType interestName);
}
