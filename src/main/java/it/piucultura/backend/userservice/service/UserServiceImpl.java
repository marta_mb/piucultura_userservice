package it.piucultura.backend.userservice.service;

import it.piucultura.backend.userservice.exception.UserNotFoundException;
import it.piucultura.backend.userservice.modelDtos.dto.UserPrincipalDto;
import it.piucultura.backend.userservice.entity.UserPrincipal;
import it.piucultura.backend.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
public class UserServiceImpl  {

    @Autowired
    UserRepository userRepository;

    /**
     * Utilizzato da Spring Security e permette la login con la username o email
     * @param usernameOrEmail
     * @return
     * @throws UserNotFoundException
     */
    @Transactional
    public UserPrincipalDto loadUserByUsername(String usernameOrEmail) throws UserNotFoundException{

        UserPrincipal userPrincipal = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .orElseThrow(() ->
                        new UserNotFoundException("UserPrincipal non trovato con username o email : " + usernameOrEmail)
                );

        return UserPrincipalDto.create(userPrincipal);
    }

    /**
     * Metodo usato da JWTAuthenticationFilter.
     * @param id
     * @return
     */
    @Transactional
    public UserPrincipalDto loadUserById(Long id) throws UserNotFoundException{

        UserPrincipal userPrincipal = userRepository.findById(id).orElseThrow(
                () -> new UserNotFoundException("UserPrincipal non trovato con id : " + id)
        );

        return UserPrincipalDto.create(userPrincipal);
    }


    @Transactional
    public UserPrincipalDto loadUserByResetToken(String resetToken) throws UserNotFoundException{

        UserPrincipal userPrincipal = userRepository.findByResetToken(resetToken).orElseThrow(
                () -> new UserNotFoundException("UserPrincipal non trovato con reset token : " + resetToken)
        );

        return UserPrincipalDto.create(userPrincipal);
    }

    @Transactional
    public UserPrincipalDto loadUserByConfirmToken(String confirmToken) throws UserNotFoundException{

        UserPrincipal userPrincipal = userRepository.findByConfirmToken(confirmToken).orElseThrow(
                () -> new UserNotFoundException("UserPrincipal non trovato con confirm token : " + confirmToken)
        );

        return UserPrincipalDto.create(userPrincipal);
    }
}