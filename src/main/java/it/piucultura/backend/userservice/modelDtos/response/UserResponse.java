package it.piucultura.backend.userservice.modelDtos.response;

import it.piucultura.backend.userservice.modelDtos.StringModel;
import it.piucultura.backend.userservice.modelDtos.dto.UserPrincipalDto;
import it.piucultura.backend.userservice.modelDtos.dto.UserSummaryDto;
import it.piucultura.backend.userservice.modelDtos.dto.JwtAuthenticationDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UserResponse {
    public static ResponseEntity<ResponseModelString> HTTP_OK_USER_STRING_MODEL(StringModel message){
        ResponseModelString genericResponse = new ResponseModelString();
        genericResponse.setError(false);
        genericResponse.setResult(message.getValue());
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }
    public static ResponseEntity<ResponseUserPrincipal> HTTP_OK_USER_PRINCIPAL_RESPONSE(UserPrincipalDto dto){
        ResponseUserPrincipal genericResponse = new ResponseUserPrincipal();
        genericResponse.setError(false);
        genericResponse.setResult(dto);
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    public static ResponseEntity<ResponseUserSummary> HTTP_OK_USER_SUMMARY_RESPONSE(UserSummaryDto dto){
        ResponseUserSummary genericResponse = new ResponseUserSummary();
        genericResponse.setError(false);
        genericResponse.setResult(dto);
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    public static ResponseEntity<ResponseCambioPassword> HTTP_OK_USER_CAMBIO_PS_RESPONSE(String dto){
        ResponseCambioPassword genericResponse = new ResponseCambioPassword();
        genericResponse.setError(false);
        genericResponse.setResult(dto);
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }
    public static ResponseEntity<ResponseCambioPassword> HTTP_ERROR_USER_CAMBIO_PS_RESPONSE(String errorMessage, String errorCode){
        ResponseCambioPassword genericResponse = new ResponseCambioPassword();
        genericResponse.setError(true);
        genericResponse.setErrorMessage(errorMessage);
        genericResponse.setErrorCode(errorCode);
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }


    public static ResponseEntity<ResponseJwtAuthentication> HTTP_OK_USER_JWT_AUTH_RESPONSE(JwtAuthenticationDTO dto){
        ResponseJwtAuthentication genericResponse = new ResponseJwtAuthentication();
        genericResponse.setError(false);
        genericResponse.setResult(dto);
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    public static ResponseEntity<ResponseModel>  HTTP_ERROR_USER(String errorMessage, String errorCode){
        ResponseModel genericResponse = new ResponseModel();
        genericResponse.setError(true);
        genericResponse.setErrorMessage(errorMessage);
        genericResponse.setErrorCode(errorCode);
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    public static ResponseEntity<ResponseJwtAuthentication> HTTP_ERROR_USER_JWT(String errorMessage, String errorCode){
        ResponseJwtAuthentication genericResponse = new ResponseJwtAuthentication();
        genericResponse.setError(true);
        genericResponse.setErrorMessage(errorMessage);
        genericResponse.setErrorCode(errorCode);
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    public static ResponseEntity<ResponseUserPrincipal> HTTP_ERROR_USER_REG(String errorMessage, String errorCode){
        ResponseUserPrincipal genericResponse = new ResponseUserPrincipal();
        genericResponse.setError(true);
        genericResponse.setErrorMessage(errorMessage);
        genericResponse.setErrorCode(errorCode);
        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }
}
