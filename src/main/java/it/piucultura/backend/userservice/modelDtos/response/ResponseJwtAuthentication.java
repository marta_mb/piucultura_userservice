package it.piucultura.backend.userservice.modelDtos.response;

import it.piucultura.backend.userservice.modelDtos.dto.JwtAuthenticationDTO;
import it.piucultura.backend.userservice.modelDtos.response.ResponseModel;

import java.util.Date;

public class ResponseJwtAuthentication extends ResponseModel {

    private JwtAuthenticationDTO result;

    public ResponseJwtAuthentication() {
    }

    public ResponseJwtAuthentication(JwtAuthenticationDTO result) {
        this.result = result;
    }

    public ResponseJwtAuthentication(String errorCode, String errorMessage, JwtAuthenticationDTO result) {
        super(errorCode, errorMessage);
        this.result = result;
    }

    public ResponseJwtAuthentication(Date timestamp, String errorCode, String errorMessage, JwtAuthenticationDTO result) {
        super(timestamp, errorCode, errorMessage);
        this.result = result;
    }

    public JwtAuthenticationDTO getResult() {
        return result;
    }

    public void setResult(JwtAuthenticationDTO result) {
        this.result = result;
    }
}
