package it.piucultura.backend.userservice.modelDtos;

public  class StringConst {
    public final static String AUTO_GENERATED_ID = "auto-generated ID";
    public final static String AUTO_GENERATED_VERSION = "auto-generated version";
    public final static String CREATED_AT = "creation date";
    public final static String UPDATED_AT = "update date";
}
