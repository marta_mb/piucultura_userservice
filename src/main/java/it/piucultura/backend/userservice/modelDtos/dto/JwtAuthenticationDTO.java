package it.piucultura.backend.userservice.modelDtos.dto;

import java.io.Serializable;

public class JwtAuthenticationDTO implements Serializable {

    private String accessToken;
    private String tokenType = "Bearer";

    public JwtAuthenticationDTO(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public JwtAuthenticationDTO(String accessToken, String tokenType) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
    }

    public JwtAuthenticationDTO() {
    }

    @Override
    public String toString() {
        return "JwtAuthenticationDTO{" +
                "accessToken='" + accessToken + '\'' +
                ", tokenType='" + tokenType + '\'' +
                '}';
    }
}
