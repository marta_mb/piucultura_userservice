package it.piucultura.backend.userservice.modelDtos.response;

import it.piucultura.backend.userservice.modelDtos.dto.UserSummaryDto;
import it.piucultura.backend.userservice.modelDtos.response.ResponseModel;

import java.util.Date;

public class ResponseUserSummary extends ResponseModel {

    private UserSummaryDto result;

    public ResponseUserSummary() {
    }

    public ResponseUserSummary(UserSummaryDto result) {
        this.result = result;
    }

    public ResponseUserSummary(String errorCode, String errorMessage, UserSummaryDto result) {
        super(errorCode, errorMessage);
        this.result = result;
    }

    public ResponseUserSummary(Date timestamp, String errorCode, String errorMessage, UserSummaryDto result) {
        super(timestamp, errorCode, errorMessage);
        this.result = result;
    }

    public UserSummaryDto getResult() {
        return result;
    }

    public void setResult(UserSummaryDto
                                  result) {
        this.result = result;
    }
}
