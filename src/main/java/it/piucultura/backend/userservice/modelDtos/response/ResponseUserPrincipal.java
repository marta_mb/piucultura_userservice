package it.piucultura.backend.userservice.modelDtos.response;

import it.piucultura.backend.userservice.modelDtos.dto.UserPrincipalDto;
import it.piucultura.backend.userservice.modelDtos.response.ResponseModel;

import java.util.Date;

public class ResponseUserPrincipal extends ResponseModel {

    private UserPrincipalDto result;

    public ResponseUserPrincipal() {
    }

    public ResponseUserPrincipal(UserPrincipalDto result) {
        this.result = result;
    }

    public ResponseUserPrincipal(String errorCode, String errorMessage, UserPrincipalDto result) {
        super(errorCode, errorMessage);
        this.result = result;
    }

    public ResponseUserPrincipal(Date timestamp, String errorCode, String errorMessage, UserPrincipalDto result) {
        super(timestamp, errorCode, errorMessage);
        this.result = result;
    }

    public UserPrincipalDto getResult() {
        return result;
    }

    public void setResult(UserPrincipalDto result) {
        this.result = result;
    }
}
