package it.piucultura.backend.userservice.modelDtos.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class RequestResetPassword {

    @NotBlank
    @Size(min = 3, max = 15)
    private String token;

    @NotBlank
    @Size(min = 6, max = 20)
    private String newPassword;



    public String getToken() {
        return token;
    }

    public void setToken(String username) {
        this.token = username;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
