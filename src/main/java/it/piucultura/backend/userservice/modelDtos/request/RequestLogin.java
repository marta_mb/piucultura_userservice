package it.piucultura.backend.userservice.modelDtos.request;

import javax.validation.constraints.NotBlank;

/**
 * Definisce il login request dtos utilizzato dalle API
 */
public class RequestLogin {

    @NotBlank
    private String usernameOrEmail;

    @NotBlank
    private String password;

    public String getUsernameOrEmail() {
        return usernameOrEmail;
    }

    public void setUsernameOrEmail(String usernameOrEmail) {
        this.usernameOrEmail = usernameOrEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
