package it.piucultura.backend.userservice.modelDtos.request;

import it.piucultura.backend.userservice.entity.Disabilita;
import it.piucultura.backend.userservice.entity.Interessi;
import it.piucultura.backend.userservice.entity.UserPrincipal;
import it.piucultura.backend.userservice.modelDtos.dto.UserPrincipalDto;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Definisce il signup request dtos utilizzato dalle API
 */
public class RequestSignUp implements Serializable {

    private Long id;

    private String name;

    private String surname;

    private String username;

    private String email;

    private String password;

    private Date birth;

    private String country;

    private String profession;

    private Collection<Disabilita> disability;

    private Collection<Interessi> interests;

    private String resetToken;

    private String confirmToken;

    private Boolean confirmEmail;

    private String qualification;


    public RequestSignUp() {
        super();
    }

    public RequestSignUp(Long id, String name, String surname, String username, String email, String password, Date birth, String country, String profession, Collection<Disabilita> disability, Collection<Interessi> interests, String resetToken, String confirmToken, Boolean confirmEmail, String qualification) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.password = password;
        this.birth = birth;
        this.country = country;
        this.profession = profession;
        this.disability = disability;
        this.interests = interests;
        this.resetToken = resetToken;
        this.confirmToken = confirmToken;
        this.confirmEmail = confirmEmail;
        this.qualification = qualification;
    }

    public static RequestSignUp create(UserPrincipal user) {

       /* List<GrantedAuthority> authorities = user.getGroups().stream().map(role ->
                new SimpleGrantedAuthority(role.getName().name())
        ).collect(Collectors.toList());*/

        List<Interessi>  interests = user.getInterests().stream().map(interessi ->
                new Interessi(interessi.getName(), interessi.getAltriInteressi())
        ).collect(Collectors.toList());

        List<Disabilita>  disability = user.getDisability().stream().map(disabilita ->
                new Disabilita(disabilita.getName(), disabilita.getAltreDisalitita())
        ).collect(Collectors.toList());

        RequestSignUp userPrincipalDto=  new RequestSignUp(
                user.getId(),
                user.getName(),
                user.getSurname(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getBirth(),
                user.getCountry(),
                user.getProfession(),
                disability,
                interests,
                user.getResetToken(),
                user.getConfirmToken(),
                user.getConfirmEmail(),
                user.getQualification()
        );
        return userPrincipalDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Collection<Disabilita> getDisability() {
        return disability;
    }

    public void setDisability(Collection<Disabilita> disability) {
        this.disability = disability;
    }

    public Collection<Interessi> getInterests() {
        return interests;
    }

    public void setInterests(Collection<Interessi> interests) {
        this.interests = interests;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public String getConfirmToken() {
        return confirmToken;
    }

    public void setConfirmToken(String confirmToken) {
        this.confirmToken = confirmToken;
    }

    public Boolean getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(Boolean confirmEmail) {
        this.confirmEmail = confirmEmail;
    }



    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Override
    public String toString() {
        return "UserPrincipalDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", birth=" + birth +
                ", country='" + country + '\'' +
                ", profession='" + profession + '\'' +
                ", disability=" + disability +
                ", interests=" + interests +
                ", resetToken='" + resetToken + '\'' +
                ", confirmToken='" + confirmToken + '\'' +
                ", confirmEmail=" + confirmEmail +
                ", qualification='" + qualification + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
